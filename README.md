<p align="center">
    <a href="atakicksmart.com">
        <img src="https://atakicksmart.com/images/logo.png">
    </a>
</p>

# ATA Kicksmart Members Site

Python Flask-based members only website to allow members of Monroe's ATA Martial Arts access to learning tools such as videos of forms, weapons and online belt materials
