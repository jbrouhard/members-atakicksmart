from flask import Flask, render_template, redirect, flash, redirect, request, session, abort
import os
import pymysql as sql
from functions_general import *

application = Flask(__name__)

db_name = 'atakicksmart_members'
db_host = 'localhost'
db_uname = 'atakicks_members'
db_pass = 'kicksmart'


@application.route("/")
def hello():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return render_template('frontpage.html')


@application.route("/forms/<id>")
def form_display(id):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        form = id
        video_id = getname(id)
        form_name = getname(id)

        form_name = form_name.replace("_", " ")

        return render_template('forms.html', **locals())


@application.route("/weapons/<id>")
def weapon_display(id):
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        form = id
        video_id = getweapon(id)
        form_name = getweapon(id)

        form_name = form_name.replace("_", " ")

        return render_template('forms.html', **locals())


# Login/Logout stuff

@application.route("/login", methods=['GET', 'POST'])
def do_login():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)
    name = request.form['username']
    password = request.form['password']
    c.execute("SELECT * FROM members WHERE username = '%s' LIMIT 1" % name)
    for row in c:
        hashed_password = row['password']
        uname = row['username']
        perms = row['perms']
        name = row['name']

    if bcrypt.checkpw(password.encode('utf-8'), hashed_password.encode('utf-8')) is True and uname == request.form['username'] and perms == 'active':
        session['logged_in'] = True
        session['username'] = uname
        session['name'] = name

        conn.close()
        return redirect('/', code=302)

    else:
        return render_template('wrongpass.html')


@application.route('/register', methods=['GET', 'POST'])
def register():
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)

    # Get form values
    username = request.form['rusername']
    password = request.form['rpassword']
    name = request.form['rname']
    email = request.form['remail']
    password = bcrypt.hashpw(password.encode('UTF-8'), bcrypt.gensalt()).decode('utf-8')

    # Generate a registeration code
    regcode = gen_regcode()

    # Execute queries.
    c.execute('INSERT INTO members (username, password, name, email, perms) VALUES ("%s", "%s", "%s", "%s", "inactive")' % (username, password, name, email))
    c.execute('INSERT INTO regcode (email, code) VALUES ("%s", "%s")' % (email, regcode))
    conn.commit()
    conn.close()
    # send_registeration(name, email, regcode) TODO: Get the function to work with localhost smtp mailing.
    thankyou = "Thank you. Your registeration is complete and the school owners have been contacted to activate your account!"
    return render_template('thankyou.html', **locals())


@application.route('/<id>')
def confirm_reg(id):
    conn = sql.connect(db_host, db_uname, db_pass, db_name)
    c = conn.cursor(sql.cursors.DictCursor)
    conn.ping(reconnect=True)

    # Lets check to see if registeration is in the db
    c.execute("SELECT email, code FROM regcode WHERE code = '%s'" % id)
    result = c.fetchone()

    for row in c:
        email = row['email']

    while result is not None:
        email = result['email']
        c.execute("UPDATE members SET perms = 'active' WHERE email = '%s'" % email)
        c.execute("DELETE FROM regcode WHERE code = '%s'" % id)
        conn.commit()
        conn.close()
        # send_activation(email)  TODO: Get the function to work with localhost smtp mailing.
        thankyou = "Thank you.   User has been activated and email sent to them"
        return render_template('thankyou.html', **locals())

    else:
       thankyou = "I'm sorry but that user cannot be found"
       return render_template('thankyou.html', **locals())


@application.route('/logout')
def logout():
    session.pop('logged_in', None)
    return redirect('/', code=302)


if __name__ == "__main__":
    application.secret_key = os.urandom(12)
    application.run(host='0.0.0.0')