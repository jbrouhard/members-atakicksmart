import bcrypt, smtplib, random
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


# This will generate the password salting
def get_hashed_password(password):
    return bcrypt.hashpw(password.encode('UTF-8'), bcrypt.gensalt())


# This will check plaintext passwording against hashed.
def check_password(password, hashed_password):
    if hashed_password == bcrypt.hashpw(password.encode('utf-8'), hashed_password.encode('utf-8')):
        return True
    else:
        return False


# Lets send email for the registeration
def send_registeration(name, email, regcode):
    fromaddr = "sender@domain.com"  # TODO: Set this as db-pulled variable, rather than explicit for security
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = email
    msg['Subject'] = "Authorize new user for Member's Site"

    body = "To authorize new user %s please click on this link:  https://members.atakicksmart.com/%s" % (name, regcode)
    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP('mail.domain.com', 587)
    server.starttls()
    server.login(fromaddr, "P@ssw0rd")  # TODO: Set this as a db-pulled variable, rather than explicit for security
    text = msg.as_string()
    server.sendmail(fromaddr, email, text)
    server.quit()

# Send user email that they have been activated
def send_activation(email):
    fromaddr = "sender@domain.com"  # TODO: Set this as db-pulled variable, rather than explicit for security
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = email
    msg['Subject'] = "Your Members Site account has been activated!"

    body = "CONGRATULATIONS!   The School owners have activated your account! /r/n /r/n Please login at https://members.atakicksmart.com !!"
    msg.attach(MIMEText(body, 'plain'))

    server = smtplib.SMTP('mail.domain.com', 587)
    server.starttls()
    server.login(fromaddr, "P@ssw0rd")  # TODO: Set this as a db-pulled variable, rather than explicit for security
    text = msg.as_string()
    server.sendmail(fromaddr, email, text)
    server.quit()


# Generate registeration code and store it in the database
def gen_regcode():
    alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    length = 64
    code = ""

    for i in range(length):
        next_index = random.randrange(len(alphabet))
        code = code + alphabet[next_index]

    return code


# Convert the ID of the link to a form identifier
def getname(id):
    name = id
    if name == 'songahm1':
        return "songahm_1"
    elif name == 'songahm2':
        return "songahm_2"
    elif name == 'songahm3':
        return "songahm_3"
    elif name == 'songahm4':
        return "songahm_4"
    elif name == 'songahm5':
        return "songahm_5"
    elif name == 'iw1':
        return "in_wha_1"
    elif name == 'iw2':
        return "in_wha_2"
    elif name == 'cj1':
        return "choong_jung_1"
    elif name == 'cj2':
        return "choong_jung_2"


# Convert the ID of the link to a weapon identifier
def getweapon(id):
    name = id
    if name == 'sshang_joel_bahng':
        return "sshang_joel_bahng"
    elif name == 'bahng_mahng_ee_woy':
        return "bahng_mahng_ee_woy"
    elif name == 'bahng_mahng_ee_half':
        return "bahng_mahng_ee_half"
    elif name == 'bahng_mahng_ee_full':
        return "bahng_mahng_ee_full"
    elif name == 'jahng_bahng':
        return "jahng_bahng"
